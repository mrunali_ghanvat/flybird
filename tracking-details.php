<!DOCTYPE html>
<html>

<head>
    <title>Flybird International Courier</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="img/flybird_favicon.png" type="image/gif" sizes="15x15">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="expires" content="0">
    <meta name="keywords" content="Courier Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <!-- css files -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/new-age.css" rel="stylesheet" type="text/css" media="all" />
    <!-- /css files -->
    <!-- font files -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700italic,700,800,800italic' rel='stylesheet' type='text/css'>
    <link href="//fonts.googleapis.com/css?family=Exo+2:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="icon" href="img/favicon.png?v=1.1" type="image/png">
</head>

<body>
    <!-- navigation -->
    <nav id="navbar-content" class="navbar navbar-default navbar-fixed-top"></nav>

    <!--HEADER-->
    <div class="header-wrapper">
        <h2 class="service-title ">TRACK YOUR SHIPMENT</h2>
     <div class="enter-tracking">
        
        
        <div class="enter-tracking-inner"><div class="enter-tracking-title"><span>Enter comma (,) seperated numbers</span></div>
        <textarea id="code" name="airwaybillno" id="" cols="30" rows="10" placeholder="Airway Bill Numbers"></textarea></div>
        
        <div class="track-btn" id="trackbutton">TRACK</div></div>

    <div id="tracking-append"></div>






</div>
    <!-- footer section -->
    <section class="footer-agileits" id="footer-content"></section>
    <!-- footer section -->

    <a href="#0" class="cd-top">Top</a>


    <!-- js files -->
    <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/index.js"></script>
    <script src="js/tracking.js"></script>
    <script src="js/top.js"></script>
    <script defer>
        $(window).load(function() {

            var html = "";
            var code = <?php echo $_GET['codes'];?>;
            var codeArray = code.split(",");
            console.log(codeArray);
            console.log(code);


            $.get("http://flybirdinternational.com/billmate/bmb/rest/index.php/tracking/gettrackingdata?v=1.4&airway_number=" + JSON.stringify(codeArray), function(responseText) {
                var response = JSON.parse(responseText);
                console.log(response);
                for (i = 0; i < codeArray.length; i++) {

                    //CHECK WHICH OF USERS CODE CORRESPPONDS TO WHICH API CODE
                    var j = response.findIndex(function(eachresponse) {
                        return codeArray[i] == eachresponse.airway_number.trim();
                    });

                    //FOR LOOP HERE
                    if (j == -1) {
                        console.log(codeArray[i]);
                        html += `<div class="tracking-wrapper">
                            <div class="bill-details" style="background-color: #f54f00;">
				<p class="bill-no-text" style="height: 25px;font-size: 15px;">Airway Bill Number</p>
				<p class="bill-no" style="font-size: 18px;">${codeArray[i]} <br><span class="error-msg">does not exist in our database</span></p>
			</div>
                            </div>`
                    } else {

                        var imgname = response[j].forwarding_company_name;
                        imgname = imgname.replace(/ /g, "");
                        imgname = imgname.toLowerCase();
                        var status = response[j].status;
                        console.log(status);
                        

                        var date = new Date(response[j].date);
                        date = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
                        
                        var dd = new Date(response[j].delivery_date);
                        dd = dd.getDate() + "-" + dd.getMonth() + "-" + dd.getFullYear();
                        
                        if (imgname=="flybird") {
                            html += `<div class="tracking-wrapper" >

			<div class="bill-details">
				<p class="bill-no-text">Airway Bill Number</p>
				<p class="bill-no">${response[j].airway_number}</p>
			</div>
			<div class="row table-wrapper">
		
		<div class="table">	<div class="table-1">
			    <div class="column">
				    <p class="label-text">Consignee Name</p>
					<p class="value-text">${response[j].consignment_name}</p>
				</div>
				<div class="column">
					<p class="label-text">Date</p>
					<p class="value-text">${date}</p>
				</div>
				<div class="column">
					<p class="label-text">Destination</p>
				    <p class="value-text">${response[j].destination}</p>
				</div>`;
                            
                if (status==null) {
                    html += `<div class="column">
					<p class="label-text">Status</p>
				    <p class="value-text">Not Available</p>
				</div>`;
                }
                else {
                      html += `<div class="column">
					<p class="label-text">Status</p>
				    <p class="value-text">${response[j].status}</p>
				</div>`;
                }
                            
				
			
                            
			 html += `</div> <div class="table-2">
			    <div class="column">
				    <p class="label-text">Forwarding Company</p>
					<p class="value-text">Flybird</p>
				</div>
				<div class="column">
					<p class="label-text">Forwarding Number</p>
					<p class="value-text">${response[j].forwarding_number}</p>
				</div>
				<div class="column">
					<p class="label-text">Delivery Date</p>
				    <p class="value-text">${dd}</p>
				</div>
				<div class="column">
					<p class="label-text">Receiver Name</p>
				    <p class="value-text">${response[j].receiver_name}</p>
				</div>
			</div></div>
				<div class="logo-details">
					<img src="img/${imgname}.png" class="fedex-img">
					<p class="forward-text">Forwarding Number: <span class="forwarding-no"> ${response[j].forwarding_number}</span></p>
				</div>
			</div>
		</div>`;
                        }
                        else {
                            
                            html += `<div class="tracking-wrapper" >

			<div class="bill-details">
				<p class="bill-no-text">Airway Bill Number</p>
				<p class="bill-no">${response[j].airway_number}</p>
			</div>
			<div class="row table-wrapper">
		
		<div class="table">	<div class="table-1">
			    <div class="column">
				    <p class="label-text">Consignee Name</p>
					<p class="value-text">${response[j].consignment_name}</p>
				</div>
				<div class="column">
					<p class="label-text">Date</p>
					<p class="value-text">${date}</p>
				</div>
				<div class="column">
					<p class="label-text">Destination</p>
				    <p class="value-text">${response[j].destination}</p>
				</div>
				<div class="column">
					<p class="label-text">Status</p>
				    <p class="value-text">${response[j].status}</p>
				</div>
			</div>
			<div class="table-2">
			    <div class="column">
				    <p class="label-text">Forwarding Company</p>
					<p class="value-text">${response[j].forwarding_company_name}</p>
				</div>
				<div class="column">
					<p class="label-text">Forwarding Number</p>
					<p class="value-text"><a href="${response[j].url}" target="_blank">${response[j].forwarding_number}</a></p>
				</div>
				<div class="column">
					<p class="label-text">Delivery Date</p>
				    <p class="value-text">${dd}</p>
				</div>
				<div class="column">
					<p class="label-text">Receiver Name</p>
				    <p class="value-text">${response[j].receiver_name}</p>
				</div>
			</div></div>
				<div class="logo-details">
					<img src="img/${imgname}.png" class="fedex-img">
					<p class="forward-text">Forwarding Number: <span class="forwarding-no"><a href="${response[j].url}" target="_blank">${response[j].forwarding_number}</a></span></p>
				</div>
			</div>
		</div>`;
                        };
                        
                    };

                };

                $('#tracking-append').html(html);

            });
        });

    </script>
    <!-- /js files -->
    <script>
        $('#navbar-content').load('navbar.html');
        $('#footer-content').load('footer.html');

    </script>
</body>

</html>
