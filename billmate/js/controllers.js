var phonecatControllers = angular.module('phonecatControllers', ['templateservicemod', 'navigationservice']);

var invoicenumber = 1;
var gohome = true;
var editinvoice = false;

/*FILTER STORING*/
var goingtoinvoice = false;
var savedfrom = "";
var savedto = "";
var savedcustomer = "";
var savedcustomercontact = "";
var savedstatus = "";

phonecatControllers.controller('menuCtrl',
    function ($scope, TemplateService, NavigationService, $http, $location, $rootScope) {
        console.log('menuctrl called');
        $rootScope.showlogout = $location.path() != '/login';
        var path = $location.path();
        $location.path(window.localStorage.getItem('user') ? path : '/login');
        $scope.goto = function (page) {
            $location.path('/' + page);
        };

        $rootScope.logout = function () {
            console.log('logout called');
            window.localStorage.clear();
            $rootScope.showlogout = false;
            $location.path('/login');

        }



    }
);
phonecatControllers.controller('loginCtrl',
    function ($scope, TemplateService, NavigationService, $http, $location, $filter, $window, $rootScope) {
        /* BASIC PAGE CONFIGURATIONS */
        $scope.template = TemplateService;
        $scope.menutitle = NavigationService.makeactive("login");
        TemplateService.title = $scope.menutitle;
        $scope.navigation = NavigationService.getnav();
        TemplateService.content = "views/login.html";
        /*===========================================*/

        $scope.user = {};
        $rootScope.showlogout = false;
        $scope.dologin = function () {
            NavigationService.dologin($scope.user).then(function (response) {
                console.log(response);
                if (response.data != 'null') {
                    window.localStorage.setItem('user', response.data.email);
                    $location.path('/home');
                } else {
                    $scope.errormessage = "Wrong email address or password !";
                };


            }, function (error) {
                console.log(error);
            });


        };

        $(window).keypress(function (event) {
            if (event.keyCode == 13) {
                $('#login-btn').click();
            }
        })



    });

phonecatControllers.controller('homeCtrl',
    function ($scope, TemplateService, NavigationService, $http, $location, $filter, $window, $rootScope) {
        /* BASIC PAGE CONFIGURATIONS */
        $scope.template = TemplateService;
        $scope.menutitle = NavigationService.makeactive("home");
        TemplateService.title = $scope.menutitle;
        $scope.navigation = NavigationService.getnav();
        TemplateService.content = "views/content.html";
        /*===========================================*/

        console.log('called');
        $rootScope.showlogout = $location.path() != '/login';
        //INITIALIZATIONS
        $scope.date = '';

        //ROUTING
        $scope.gotocreate = function () {
            $location.path('/create');
        };


        //Logout functionality



        //FUNCTIONS
        //GET LAST BACKUP DATE
        var getlastbackupdatesuccess = function (data, status) {
            console.log(data);
            $scope.date = data;
        };
        var getlastbackupdateerror = function (data, status) {
            console.log(data);
        };
        $scope.getlastbackupdate = function () {
            NavigationService.getlastbackupdate().success(getlastbackupdatesuccess).error(getlastbackupdateerror);
        };

        //BACKUP
        var backupdbrecordsuccess = function (data, status) {
            $window.open('http://localhost/billmate/backup.php?name=flybird_backup_' + $scope.timestamp + '.sql', '_blank');
            console.log(data);
            $scope.getlastbackupdate();
        };
        var backupdbrecorderror = function (data, status) {

        };
        $scope.backupdb = function () {
            var time = new Date();
            $scope.timestamp = $filter('date')(time, 'dd-MM-yyyy:HH-mm-s');
            NavigationService.backupdbrecord({
                'name': 'flybird_backup_' + $scope.timestamp + '.sql'
            }).success(backupdbrecordsuccess).error(backupdbrecorderror);
        };

        //INITIAL FUNCTION CALLS
        $scope.getlastbackupdate();


    }
);
phonecatControllers.controller('statsCtrl', ['$scope', 'TemplateService', 'NavigationService', '$interval',
    function ($scope, TemplateService, NavigationService, $interval) {

        /*BASIC CONFIGURATIONS*/
        $scope.template = TemplateService;
        $scope.menutitle = NavigationService.makeactive("stats");
        TemplateService.title = $scope.menutitle;
        $scope.navigation = NavigationService.getnav();
        TemplateService.content = "views/stats.html";
        /*===========================================*/


        /*INITIAlIZATIONS*/
        $scope.stats = [];
        $scope.colors = ['#00796b', '#ffa000', '#ff8e72'];


        $scope.filter = {};
        $scope.filter.company = null;



        var todaysdate = new Date();
        var pastdate = new Date();
        pastdate.setDate(pastdate.getDate() - 60);
        console.log(todaysdate.getYear());
        var td = todaysdate.getDate() + '/' + (todaysdate.getMonth() + 1) + '/' + todaysdate.getFullYear();
        var fd = pastdate.getDate() + '/' + (pastdate.getMonth() + 1) + '/' + pastdate.getFullYear();

        $scope.filteroptions = {
            'fromdate': fd,
            'todate': td,
            'customer': '',
            'status': ''
        };

        $scope.statname = "Total Sales";
        $scope.stattitle = "From " + $scope.filteroptions.fromdate + " to " + $scope.filteroptions.todate;
        $scope.statvalue = 0;

        //INITIAL 30 DAYS
        $scope.labels = [];
        $scope.data = [];
        $scope.data.push([]);
        $scope.data.push([]);
        var date = new Date();
        var noofdaysinmonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();



        //FUNTIONS
        //MAKE THE CHART
        $scope.datasetOverride = [
            {
                label: "Bar chart",
                borderWidth: 1,
                type: 'bar'
            },
            {
                label: "Line chart",
                borderWidth: 3,
                hoverBackgroundColor: "rgba(255,99,132,0.4)",
                hoverBorderColor: "rgba(255,99,132,1)",
                type: 'line'
            }
        ];

        /*RESET ALL TOTALS TO 0*/
        var resettotals = function () {
            $scope.labels = [];
            $scope.data = [];
            $scope.data.push([]);
            $scope.data.push([]);
            $scope.totalsales = 0;
            $scope.netsales = 0;
            $scope.servicetax = 0;
            $scope.sbtax = 0;
            $scope.kkctax = 0;
            $scope.totalpaid = 0;
            $scope.totalunpaid = 0;
        };

        /*
        $('.aab').animate({ scrollTop: $('.aab').prop("scrollHeight") - $('.aab').height() }, 1000);
        */
        /*GET STATS*/
        var getstatssuccess = function (data, status) {
            $('.bs-example-modal-sm').modal('hide');
            console.log(data);
            $scope.stats = data;
            resettotals();

            //WITHOUT OFF DAYS
            for (var t = 0; t < $scope.stats.length; t++) {
                $scope.totalsales += parseFloat($scope.stats[t].total);
                $scope.netsales += parseFloat($scope.stats[t].amount);
                $scope.servicetax += parseFloat($scope.stats[t].service_tax);
                $scope.sbtax += parseFloat($scope.stats[t].swachhbharat_tax);
                $scope.kkctax += parseFloat($scope.stats[t].krishikalyancess_tax);
                $scope.totalpaid += parseFloat($scope.stats[t].paid);
                $scope.totalunpaid += parseFloat($scope.stats[t].total) - parseFloat($scope.stats[t].paid);

                $scope.labels.push($scope.stats[t].date);
                $scope.data[0].push($scope.stats[t].total);
                $scope.data[1].push($scope.stats[t].total);
            };
            $scope.statvalue = $scope.totalsales;
            $scope.stattitle = "From " + $scope.filteroptions.fromdate + " to " + $scope.filteroptions.todate;
            if ($scope.filteroptions.customer != '') {
                $scope.stattitle += " of " + $scope.filteroptions.customer;
            };
            if ($scope.filteroptions.status != '') {
                if ($scope.filteroptions.status == 1) {
                    var stattitle = "Paid";
                } else {
                    var stattitle = "Unpaid";
                };
                $scope.stattitle += " Where Status is " + stattitle;
            };

            //WITH OFF DAYS
            var dd = new Date($scope.filteroptions.fromdate);
            console.log($scope.filteroptions.fromdate);
        };
        var getstatserror = function (data, status) {
            $('.bs-example-modal-sm').modal('hide')
            $scope.reports = false;
        };
        $scope.getstats = function (fromdate, todate, customer, status, filter) {
            if (filter == true) {
                fromdate = $('input[name=fromdate]').val();
                todate = $('input[name=todate]').val();
            };
            console.log($scope.filteroptions);
            console.log(customer);
            console.log(status);
            NavigationService.getdatewisestats(fromdate, todate, customer, status).success(getstatssuccess).error(getstatserror);
        };

        //SELECT DIFFERENT STAT
        $scope.statgraphvalues = function (obj) {
            $scope.data = [];
            $scope.data.push([]);
            $scope.data.push([]);
            for (var t = 0; t < $scope.stats.length; t++) {
                if (obj == "unpaid" || obj == "totaltax") {
                    if (obj == "unpaid") {
                        $scope.data[0].push(parseFloat($scope.stats[t].total) - parseFloat($scope.stats[t].paid));
                        $scope.data[1].push(parseFloat($scope.stats[t].total) - parseFloat($scope.stats[t].paid));
                    } else {
                        $scope.data[0].push(parseFloat($scope.stats[t].service_tax) + parseFloat($scope.stats[t].swachhbharat_tax) + parseFloat($scope.stats[t].krishikalyancess_tax));
                        $scope.data[1].push(parseFloat($scope.stats[t].service_tax) + parseFloat($scope.stats[t].swachhbharat_tax) + parseFloat($scope.stats[t].krishikalyancess_tax));
                    };
                } else {
                    $scope.data[0].push($scope.stats[t][obj]);
                    $scope.data[1].push($scope.stats[t][obj]);
                };

            };
            console.log($scope.data[0]);

        };
        $scope.statselect = function (statval, statname, obj) {
            console.log($scope.stats);
            $scope.statgraphvalues(obj);
            $scope.statvalue = statval;
            $scope.statname = statname;
        };

        //GETTING LIST OF ALL CUSTOMER
        var getallcustomerssuccess = function (data, status) {
            console.log(data);
            $scope.customers = data;
        };
        var getallcustomerserror = function (data, status) {
            $scope.customers = false;
        };
        $scope.getallcustomers = function () {
            NavigationService.getallcustomers().success(getallcustomerssuccess).error(getallcustomerserror);
        };

        //PUT FILTER
        $scope.nametyped = function () {
            $scope.filter.company = $scope.filteroptions.customer;
            if ($scope.filteroptions.customer == '') {
                $scope.filteroptions.customerid = '';
            };
            //  console.log($scope.filter.company);
        };

        //RESET FILTEER
        $scope.resetfilter = function () {
            $interval(function () {
                $scope.filter.company = null
            }, 200, 1);
        };

        //CUSTOMER SELECTED
        $scope.customerselected = function (cust) {
            $scope.filter.company = null;
            $scope.filteroptions.customerid = cust.id;
            $scope.filteroptions.customer = cust.company;
        };

        /*INITIAL FUNCTION CALLS*/
        resettotals();
        $scope.getstats($scope.filteroptions.fromdate, $scope.filteroptions.todate);
        $scope.getallcustomers();

    }
]);
phonecatControllers.controller('reportsCtrl', ['$scope', 'TemplateService', 'NavigationService', '$interval', '$location',
    function ($scope, TemplateService, NavigationService, $interval, $location) {
        $scope.template = TemplateService;
        $scope.menutitle = NavigationService.makeactive("reports");
        TemplateService.title = $scope.menutitle;
        $scope.navigation = NavigationService.getnav();
        TemplateService.content = "views/reports.html";

        /*INITIALIZATIONS*/
        $scope.reports = [];
        $scope.tracking = {};
        $scope.filter = {};
        $scope.filter.company = null;

        $scope.invoicenumberreference = "";


        $scope.payment = {
            'selectedpaid': 0,
            'selectedtotal': 0,
            'selectedtds': 0,
            'status': 0,
            'tds': 0
        };

        $scope.errormsg = 'There is an error';

        $scope.reportprogress = 0;

        var todaysdate = new Date();
        var pastdate = new Date();
        pastdate.setDate(pastdate.getDate() - 60);
        console.log(todaysdate.getYear());
        var td = todaysdate.getDate() + '/' + (todaysdate.getMonth() + 1) + '/' + todaysdate.getFullYear();
        var fd = pastdate.getDate() + '/' + (pastdate.getMonth() + 1) + '/' + pastdate.getFullYear();

        $scope.filteroptions = {
            'fromdate': fd,
            'todate': td,
            'customer': '',
            'customer_contact': '',
            'status': ''
        };



        /*BUTTON CLICK FOR INSERTING TRACKING RECORD*/

        $scope.closemainmodal = function () {
            $scope.tracking = {};
            NavigationService.trackinggetoneby($scope.reports[$scope.reportselectedindex].id).then(function (response) {
                console.log(response);

                if (response.data != 'null') {
                    $scope.tracking = response.data;
                }
            }, function (error) {
                console.log(error);
            })



            //            GET ALL COURIER SERVICES 
            NavigationService.getcourierservices().then(function (response) {
                console.log(response);
                $scope.courierservices = response.data;
            }, function (error) {
                console.log(error);
            })

            $('.bs-example-modal-sm').modal('hide');

        }



        /*INSERT TRACKING RECORDS*/
        $scope.inserttrackrecords = function () {

            console.log($scope.tracking);
            if ($scope.tracking.id) {
                //Update
                NavigationService.editbyid($scope.tracking).then(function (response) {
                    console.log('Updated record successfully !');
                    console.log(response);
                }, function (error) {
                    console.log(error);
                });

            } else {
                //Insert
                $scope.tracking.invoice_id = $scope.reports[$scope.reportselectedindex].id;
                NavigationService.insert(angular.toJson($scope.tracking)).then(function (response) {
                    console.log('inserted record successfully !');
                    console.log(response);
                }, function (error) {
                    console.log(error);
                });



            }
            $('.tracking').modal('hide');
            $('.modal-backdrop ').remove();


        }

        //$scope.reports[$scope.reportselectedindex].id
        /*INITIAL FUNCTIONS*/

        /*RESET ALL TOTALS TO 0*/
        var resettotals = function () {
            $scope.totalsales = 0;
            $scope.netsales = 0;
            $scope.servicetax = 0;
            $scope.sbtax = 0;
            $scope.kkctax = 0;
            $scope.totalpaid = 0;
            $scope.totalunpaid = 0;
        };
        var settotals = function () {
            console.log($scope.reports);
            for (var t = 0; t < $scope.reports.length; t++) {
                $scope.totalsales += parseFloat($scope.reports[t].total);
                $scope.netsales += parseFloat($scope.reports[t].amount);
                $scope.servicetax += parseFloat($scope.reports[t].service_tax);
                $scope.sbtax += parseFloat($scope.reports[t].swachhbharat_tax);
                $scope.kkctax += parseFloat($scope.reports[t].krishikalyancess_tax);
                $scope.totalpaid += parseFloat($scope.reports[t].paid);
                $scope.totalunpaid += parseFloat($scope.reports[t].total) - parseFloat($scope.reports[t].paid);
            };
        };

        /*KEYBOARD*/


        /*GET REPORTS*/
        var getreportssuccess = function (data, status) {

            console.log(data);

            $('.loadingmodal').modal('hide');

            $scope.reportprogress = 75;
            //CLEAR SAVED VARIABLES
            savedfrom = $scope.filteroptions.fromdate;
            savedto = $scope.filteroptions.todate;
            savedcustomer = $scope.filteroptions.customer;
            savedcustomercontact = $scope.filteroptions.customer_contact;
            savedstatus = $scope.filteroptions.status;

            $scope.reportprogress = 88;

            $scope.reports = data;
            resettotals();
            settotals();
            $scope.reportprogress = 100;
        };
        var getreportserror = function (data, status) {

            $('.loadingmodal').modal('hide');

            $('.bs-example-modal-sm2').modal('toggle')
            $scope.reports = false;
        };
        $scope.getreports = function (fromdate, todate, customer, customer_contact, status, filter) {
            $scope.reportprogress = 0;

            $('.loadingmodal').modal('show');

            if (filter == true) {
                fromdate = $('input[name=start]').val();
                todate = $('input[name=end]').val();
                $scope.filteroptions.todate=todate;
                $scope.filteroptions.fromdate=fromdate;
            };

            if (goingtoinvoice == true) {
                fromdate = savedfrom;
                todate = savedto;
                customer = savedcustomer;
                customer_contact = savedcustomercontact;
                status = savedstatus;
                goingtoinvoice = false;
            };

            $scope.reportprogress = 25;
            console.log(fromdate);
            NavigationService.getreports(fromdate, todate, customer, customer_contact, status).success(getreportssuccess).error(getreportserror);
            $scope.reportprogress = 50;
        };


        /*FUNCTIONS*/
        //GETTING LIST OF ALL CUSTOMER
        var getallcustomerssuccess = function (data, status) {
            console.log(data);
            $scope.customers = data;
        };
        var getallcustomerserror = function (data, status) {
            $scope.customers = false;
        };
        $scope.getallcustomers = function () {
            NavigationService.getallcustomers().success(getallcustomerssuccess).error(getallcustomerserror);
        };
        var listitems;
        var selectedli = 0;
        //PUT FILTER
        $scope.nametyped = function () {
            console.log('name typed called');
            $('.tablediv').addClass('inheritclass');

            $scope.filter.company = $scope.filteroptions.customer;
            //            console.log($scope.filter.company);

            if ($scope.filteroptions.customer == '') {
                $scope.getreports($scope.filteroptions.fromdate, $scope.filteroptions.todate, $scope.filteroptions.customer, $scope.filteroptions.customer_contact, $scope.filteroptions.status, true);
            };



            selectedli = -1;


        };

        //RESET FILTEER
        $scope.resetfilter = function () {

            $('.tablediv').removeClass('inheritclass');

            $interval(function () {
                $scope.filter.company = null
            }, 200, 1);
        };




        /*Arrow key functionality*/
        $(document).keydown(function (e) {

            if (e.keyCode == 38 || e.keyCode == 40) {
                e.preventDefault();
            }

            listitems = $('#company-suggestions ul li').length;
            //            console.log('length', listitems);

            // ARROW DOWN
            if (e.keyCode == 40) {
                selectedli = selectedli + 1 == listitems ? 0 : selectedli + 1;
                $('#company-suggestions ul li').removeClass('selected');
                $('#listitem' + selectedli).addClass('selected');
                //                console.log('down', selectedli);




            } else if (e.keyCode == 38) //ARROW UP
            {

                //                console.log('before', selectedli);
                selectedli = selectedli - 1 < 0 ? listitems - 1 : selectedli - 1;
                //                console.log('after', selectedli);
                $('#company-suggestions ul li').removeClass('selected');
                $('#listitem' + selectedli).addClass('selected');
            } else if (e.keyCode == 13) //Enter is pressed
            {
                console.log('Enter is pressed');
                if ($("#company-name").is(":focus")) {
                    console.log($('#listitem' + selectedli));
                    $('#listitem' + selectedli).click();

                }

            }
        });


        /*WHEN A CUSTOMER IS CLICKED*/
        $scope.customerselected = function (cust) {
            console.log('Customer selected ', cust);
            $('.tablediv').removeClass('inheritclass');

            $scope.filter.company = null;
            $scope.filteroptions.customer = cust.company;
            $scope.filteroptions.customer_contact = cust.contact;

            $scope.getreports($scope.filteroptions.fromdate, $scope.filteroptions.todate, $scope.filteroptions.customer, $scope.filteroptions.customer_contact, $scope.filteroptions.status, true);

        };

        //SET ID WHEN ACTIONS OPENED
        $scope.reportaction = function (index) {
            $scope.reportselectedindex = index;
            $scope.invoicenumberreference = $scope.reports[index].bill_no;
            $('.bs-example-modal-sm').modal('show');
            //            $scope.payment.paid = parseFloat($scope.reports[$scope.reportselectedindex].paid);
            $scope.payment.selectedpaid = parseFloat($scope.reports[$scope.reportselectedindex].paid);
            $scope.payment.tds = parseFloat($scope.reports[$scope.reportselectedindex].tds);

            $scope.payment.selectedtotal = parseFloat($scope.reports[$scope.reportselectedindex].total);
            console.log(parseFloat($scope.reports[$scope.reportselectedindex].total) - parseFloat($scope.reports[$scope.reportselectedindex].paid));
            console.log($scope.payment);
        };

        //OPEN INVOICE PAGE
        $scope.openinvoice = function () {
            invoicenumber = $scope.reports[$scope.reportselectedindex].id;
            gohome = false;
            $('.bs-example-modal-sm').modal('hide');
            $interval(function () {
                goingtoinvoice = true;

                //STORING FOR REFRESH
                savedfrom = $scope.filteroptions.fromdate;
                savedto = $scope.filteroptions.todate;
                savedcustomer = $scope.filteroptions.customer;
                savedcustomercontact = $scope.filteroptions.customer_contact;
                savedstatus = $scope.filteroptions.status;

                $location.path('/invoice');
            }, 500, 1);
        };

        //EDIT INVOICE
        $scope.editinvoice = function () {
            editinvoice = true;
            invoicenumber = $scope.reports[$scope.reportselectedindex].id;
            $('.bs-example-modal-sm').modal('hide');

            $location.path('/create');
        };

        //DELETE REPORT
        var deleteinvoicesuccess = function (data, status) {
            $scope.reports.splice($scope.reportselectedindex, 1);
            resettotals();
            settotals();
            $scope.reportselectedindex = null;
            $('.suremodal').modal('hide');
            $('.bs-example-modal-sm').modal('hide');
        };
        var deleteinvoiceerror = function (data, status) {
            //ERROR MESSAGE - Some Problem While Deleting Report. Try Again.
        };
        $scope.deleteconfirm = function () {
            NavigationService.deleteinvoice($scope.reports[$scope.reportselectedindex].id).success(deleteinvoicesuccess).error(deleteinvoiceerror);
        };
        $scope.deletereport = function () {
            //ARE YOU SURE
            $('.suremodal').modal('show');
        };

        //PAYMENT
        $scope.makefullpayment = function () {
            $scope.payment.selectedpaid = $scope.payment.selectedtotal - $scope.payment.tds;
        };

        $scope.addingtds = function () {
            console.log('adding TDS');
        };

        $scope.openpaymentmodal = function () {
            $('.bs-example-modal-sm').modal('hide');
            $('.paymentmodal').modal('show');
        };

        //UPDATE PAYMENT
        var updatepaymentsuccess = function (data, status) {
            goingtoinvoice = true;
            $('.paymentmodal').modal('hide');
            //$('.bs-example-modal-sm').modal('hide');
            //$('.modal-backdrop').remove();
            $scope.reports[$scope.reportselectedindex].paid = parseFloat($scope.payment.selectedpaid);
            //$scope.reportselectedindex = null;
            //REFRESH PAGE WITH FILTERS
            $scope.getreports();
        };
        var updatepaymenterror = function (data, status) {
            //ERROR
        };

        //EDIT TDS
        var updatenewtds = function (int) {
            console.log('TDS edited');

            var updatetdssuccess = function (data, status) {
                console.log(data, status);
                if (int == 1) {
                    goingtoinvoice = true;
                    $('.paymentmodal').modal('hide');
                    $scope.reports[$scope.reportselectedindex].tds = parseFloat($scope.payment.tds);
                    //$scope.reportselectedindex = null;
                    //REFRESH PAGE WITH FILTERS
                    $scope.getreports();
                };

            };
            var updatetdserror = function (data, status) {

            };
            NavigationService.updatetds($scope.reports[$scope.reportselectedindex].id, $scope.payment.tds).success(updatetdssuccess).error(updatetdserror);
        };


        $scope.updatepayment = function () {
            console.log($scope.payment.selectedpaid)
            if ($scope.payment.selectedpaid > 0) {
                console.log('paid something');
                if (parseFloat($scope.reports[$scope.reportselectedindex].total) == (parseFloat($scope.payment.selectedpaid) + parseFloat($scope.payment.tds))) {
                    obj = {
                        'paid': parseFloat($scope.payment.selectedpaid),
                        'status_id': 1
                    };
                } else {
                    obj = {
                        'paid': parseFloat($scope.payment.selectedpaid),
                        'status_id': 2
                    };
                };
                NavigationService.updatepayment($scope.reports[$scope.reportselectedindex].id, obj).success(updatepaymentsuccess).error(updatepaymenterror);

                if ($scope.reports[$scope.reportselectedindex].tds != $scope.payment.tds) {
                    updatenewtds(0);
                };

            } else {
                if ($scope.reports[$scope.reportselectedindex].tds != $scope.payment.tds) {
                    updatenewtds(1);
                };
            };

        };


        //CONVERT JSON TO CSV
        $scope.getcsv = function (JSONData, ReportTitle, ShowLabel) {

            console.log(JSONData);
            var csgst = 0;
            var igst = 0;

            for (var j = 0; j < JSONData.length; j++) {
                delete JSONData[j]["id"];
                delete JSONData[j]["$$hashKey"];
                delete JSONData[j]["type"];
                delete JSONData[j]["weight"];
                delete JSONData[j]["customer_id"];
                delete JSONData[j]["customer_name"];
                delete JSONData[j]["customer_contact"];
                delete JSONData[j]["customer_address"];
                delete JSONData[j]["krishikalyancess_tax"];
                delete JSONData[j]["swachhbharat_tax"];

                if (JSONData[j].gsttype == '1') {
                    JSONData[j].cgst = JSONData[j].service_tax / 2;
                    JSONData[j].sgst = JSONData[j].service_tax / 2;
                    csgst += parseFloat(JSONData[j].service_tax);
                    JSONData[j].igst = 0;
                } else {
                    JSONData[j].cgst = 0;
                    JSONData[j].sgst = 0;
                    JSONData[j].igst = JSONData[j].service_tax;
                    igst += parseFloat(JSONData[j].service_tax);
                };


                delete JSONData[j]["service_tax"];
                delete JSONData[j]["gsttype"];

                //delete myObject["keyname"];
            };

            console.log(JSONData);

             JSONData.push({
                'bill_no': '',
                'date': '',
               
                'paid': 'TOTAL (paid)',
                'amount': 'TOTAL (amount)',
                
                'cgst': 'TOTAL (CGST)',
                'sgst': 'TOTAL (SGST)',
                'igst': 'TOTAL (IGST)',
                'tds': '',
                'total': 'TOTAL',
                'customer_company': '',
                'status': '',
                
                 
            });

            JSONData.push({
                'bill_no': '',
                'date': '',
                
                'paid': $scope.totalpaid,
                'amount': $scope.netsales,
              
                'cgst': csgst / 2,
                'sgst': csgst / 2,
                'igst': igst,
                  'tds': '',
                'total': $scope.totalsales,
                'customer_company': '',
                'status': '',
               
                 
            });

            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';
            //Set Report title in first row or line
            var title = 'Flybird Invoice Reports from ' + $scope.filteroptions.fromdate + ' to ' + $scope.filteroptions.todate;
            if ($scope.filteroptions.customer != "") {
                title += ' OF ' + $scope.filteroptions.customer;
            };
            if ($scope.filteroptions.status != "") {
                title += ' WITH ' + $scope.filteroptions.status + ' Status ';
            };

            CSV += title + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = title;
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension    

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = uri;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        };

        //CONVERT JSON TO CSV
        $scope.getcustomercsv = function (reportsdata, ReportTitle, ShowLabel) {

            console.log(reportsdata);

            var JSONData = [];
            var n = 1;
            for (var j = 0; j < reportsdata.length; j++) {

                if (j == 0) {
                    if (reportsdata[j].gsttype == '1') {
                        var cgst = parseFloat(reportsdata[j].service_tax) / 2;
                        var igst = 0;
                    } else {
                        var igst = parseFloat(reportsdata[j].service_tax);
                        var cgst = 0;
                    };

                    JSONData.push({
                        'Sr': n,
                        'customer_company': reportsdata[j].customer_company,
                        'amount': reportsdata[j].amount,
                        'cgst': cgst,
                        'sgst': cgst,
                        'igst': igst,
                        'total': reportsdata[j].total,
                        'tds': reportsdata[j].tds,
                        'paid': reportsdata[j].paid,
                        'unpaid': parseFloat(reportsdata[j].total) - parseFloat(reportsdata[j].tds) - parseFloat(reportsdata[j].paid)
                    });
                    console.log(JSONData);
                    n++;
                } else {
                    var check = false;
                    /*CHECK IF CURRENT ARRAY HAS THIS COMPANY*/
                    for (var w = 0; w < JSONData.length; w++) {
                        if (JSONData[w].customer_company == reportsdata[j].customer_company) {

                            if (reportsdata[j].gsttype == '1') {
                                cgst = parseFloat(reportsdata[j].service_tax) / 2;
                                igst = 0;
                            } else {
                                igst = parseFloat(reportsdata[j].service_tax);
                                cgst = 0;
                            };

                            JSONData[w].amount = parseFloat(JSONData[w].amount) + parseFloat(reportsdata[j].amount);
                            JSONData[w].cgst = JSONData[w].cgst + cgst;
                            JSONData[w].sgst = JSONData[w].sgst + cgst;
                            JSONData[w].igst = JSONData[w].igst + igst;
                            JSONData[w].total = parseFloat(JSONData[w].total) + parseFloat(reportsdata[j].total);
                            JSONData[w].tds = parseFloat(JSONData[w].tds) + parseFloat(reportsdata[j].tds);
                            JSONData[w].paid = parseFloat(JSONData[w].paid) + parseFloat(reportsdata[j].paid);
                            var thisunpaid = parseFloat(reportsdata[j].total) - parseFloat(reportsdata[j].tds) - parseFloat(reportsdata[j].paid);
                            JSONData[w].unpaid = parseFloat(JSONData[w].unpaid) + thisunpaid;
                            check = true;

                            break;
                        };
                    };
                    if (check == false) {

                        if (reportsdata[j].gsttype == '1') {
                            cgst = parseFloat(reportsdata[j].service_tax) / 2;
                            igst = 0;
                        } else {
                            igst = parseFloat(reportsdata[j].service_tax);
                            cgst = 0;
                        };

                        JSONData.push({
                            'Sr': n,
                            'customer_company': reportsdata[j].customer_company,
                            'amount': reportsdata[j].amount,
                            'cgst': cgst,
                            'sgst': cgst,
                            'igst': igst,
                            'total': reportsdata[j].total,
                            'tds': reportsdata[j].tds,
                            'paid': reportsdata[j].paid,
                            'unpaid': parseFloat(reportsdata[j].total) - parseFloat(reportsdata[j].tds) - parseFloat(reportsdata[j].paid)
                        });
                        n++;
                    };
                };

            };

            console.log(JSONData);

            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';
            //Set Report title in first row or line
            var title = 'Flybird Customer Reports from ' + $scope.filteroptions.fromdate + ' to ' + $scope.filteroptions.todate;

            CSV += title + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = title;
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension    

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = uri;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        };


        /*INITIAL FUNCTION CALLS*/
        resettotals();
        $scope.getreports($scope.filteroptions.fromdate, $scope.filteroptions.todate);
        $scope.getallcustomers();









    }
]);
phonecatControllers.controller('addcustomerCtrl', ['$scope', 'TemplateService', 'NavigationService',
    function ($scope, TemplateService, NavigationService) {
        $scope.template = TemplateService;
        $scope.menutitle = NavigationService.makeactive("addcustomer");
        TemplateService.title = $scope.menutitle;
        $scope.navigation = NavigationService.getnav();
        TemplateService.content = "views/addcustomer.html";

        /*INITIALIZATIONS*/
        $scope.addform = true;
        $scope.customers = [];
        $scope.customerinfo = {};

        /* INITIALLY GETTING LIST OF CUSTOMERS FROM DATABASE */
        var getallcustomerssuccess = function (data, status) {
            $scope.customers = data;
        };
        var getallcustomerserror = function (data, status) {
            $scope.customers = false;
        };
        $scope.getallcustomers = function () {
            NavigationService.getallcustomers().success(getallcustomerssuccess).error(getallcustomerserror);
        };
        $scope.getallcustomers();

        $scope.colors = ['#8E24AA', '#e53935', '#5E35B1', '#3949AB', '#FFB300', '#6D4C41', '#546E7A', '#3F51B5', '#9E9E9E', '#CDDC39'];
        /*ROUTING*/


        /*REUSABLES*/

        var getalphanumeric = function (alpha) {
            var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            return letters.indexOf(alpha);
        };

        /*FUNCTIONS*/
        $scope.getletter = function (name) {
            var firstletter = name.charAt(0);
        };
        $scope.getcolor = function (name) {
            var nameval = 0;
            for (var w = 0; w < name.length; w++) {
                if (name.charAt[w] != '') {
                    var val = getalphanumeric(name.charAt(w));
                    nameval += val;
                };
            };
            return $scope.colors[nameval % 10];
        };

        /*WHEN CUSTOMER SELECTED*/
        $scope.editcustomer = function (ind) {
            $scope.addform = false;
            $scope.customerinfo = $scope.customers[ind];
        };
        /*WHEN EDIT CANCELLED*/
        $scope.canceledit = function () {
            $scope.customerinfo = {};
            $scope.addform = true;
        };

        /*WHEN CUSTOMER SAVED*/
        var editcustomersuccess = function (data, status) {
            $scope.addform = true;
            $scope.customerinfo = {};
        };
        var editcustomererror = function (data, status) {
            //ERROR MESSAGE (COULD NOT EDIT, TRY AGAIN)
            $scope.errormsg = "The system could not edit the customer , please try again"
            $('#errorModal').modal('show');
        };
        $scope.savecustomer = function (id) {
            /*Edit in dB where id = id, */
            NavigationService.editcustomer(id, $scope.customerinfo).success(editcustomersuccess).error(editcustomererror);
        };


        /*WHEN CUSTOMER ADDED*/
        var addcustomersuccess = function (data, status) {
            console.log(data);
            if (data == "false") {
                //ERROR MESSAGE (CUSTOMER ALREADY EXISTS)
                $scope.errormsg = "This customer is already registered in the system, cannot overwrite."
                $('#errorModal').modal('show');
            } else {
                //CALL GET ALL CUSTOMERS TO REFRESH LIST
                $scope.getallcustomers();
                //CLEAN FORM
                $scope.customerinfo = {};
            };
        };
        var addcustomererror = function (data, status) {
            //ERROR MESSAGE (COULD NOT ADD CUSTOMER, CHECK NETWORK CONNECTION)
            $scope.errormsg = "The system could not add the customer , please try again"
            $('#errorModal').modal('show');
        };
        $scope.addcustomer = function () {
            NavigationService.checkcustomer($scope.customerinfo).success(addcustomersuccess).error(addcustomererror);
        };

        //DELETE CUSTOMER BY ID
        var deletecustomerbyidsuccess = function (data, status) {
            $scope.idtodelete = null;
            console.log(data);
            $scope.getallcustomers();
            $scope.customers = [];
            $('.suremodal').modal('hide');
        };
        var deletecustomerbyiderror = function (data, status) {
            console.log(data);
        };
        $scope.deleteconfirm = function () {
            NavigationService.deletecustomerbyid($scope.idtodelete).success(deletecustomerbyidsuccess).error(deletecustomerbyiderror);
        };
        $scope.deletecustomerbyid = function (id) {
            $('.suremodal').modal('show');
            $scope.idtodelete = id;
        };
    }
]);
phonecatControllers.controller('settingsCtrl', ['$scope', 'TemplateService', 'NavigationService',
    function ($scope, TemplateService, NavigationService) {
        $scope.template = TemplateService;
        $scope.menutitle = NavigationService.makeactive("settings");
        TemplateService.title = $scope.menutitle;
        $scope.navigation = NavigationService.getnav();
    }
]);
phonecatControllers.controller('pixoloCtrl', ['$scope', 'TemplateService', 'NavigationService',
    function ($scope, TemplateService, NavigationService) {
        $scope.template = TemplateService;
        $scope.menutitle = NavigationService.makeactive("pixolo");
        TemplateService.title = $scope.menutitle;
        $scope.navigation = NavigationService.getnav();
    }
]);
phonecatControllers.controller('createCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', '$interval', '$filter',
    function ($scope, TemplateService, NavigationService, $location, $interval, $filter) {
        $scope.template = TemplateService;
        TemplateService.title = $scope.menutitle;
        $scope.navigation = NavigationService.getnav
        TemplateService.content = "views/create.html";

        //INITIALIZATION
        var item = {
            'product_id': '1',
            'date': '',
            'weight': 0,
            'unit_amount': 0,
            'pieces': 0,
            'from_place': '',
            'to_place': '',
            'reciever_contact': '',
            'carrier': '',
            'total': 0,
            'taxed': true,
            'sync': false
        };
        $scope.items = [];
        $scope.items.push(item);

        $scope.customer = {
            'name': null,
            'contact': '',
            'address': '',
            'company': ''
        };
        $scope.filter = {};
        $scope.filter.company = null;

        //DATEE
        $scope.date = new Date();

        //INVOICE FIELD
        $scope.invoice = {
            'bill_no': '',
            'customer_id': '',
            'status_id': '',
            'user_id': 1, //ADDING CONSTANT USER (CHANGE WHEN LOGIN INTEGRATED
            'date': $filter('date')($scope.date, 'yyyy-MM-dd'),
            'paid': 0,
            'type': '',
            'gsttype': 1,
            'amount': 0,
            'service_tax': 0,
            'swachhbharat_tax': 0,
            'krishikalyancess_tax': 0,
            'total': 0,
            'tds': 0,
            'customer_name': $scope.customer.name,
            'from_date': '',
            'to_date': ''
        };

        if (editinvoice) {

            var getinvoicesuccess = function (data, status) {
                console.log(data);

                //DATEE
                $scope.date = data.date;

                $scope.items = [];
                for (var ip = 0; ip < data.products.length; ip++) {
                    data.products[ip].date = new Date(data.products[ip].date);

                    console.log(data.products[ip]);

                    //MAKE VALUE
                    if (data.products[ip].taxed == '1') {
                        data.products[ip].taxed = true;
                        data.products[ip].total = parseFloat(data.products[ip].total) + (18 / 100) * parseFloat(data.products[ip].total);
                    } else {
                        data.products[ip].taxed = false;
                    };

                    console.log(data.products[ip].total);

                    data.products[ip].sync = true;
                    $scope.items.push(data.products[ip]);
                    $scope.items[$scope.items.length - 1].to_place = data.products[ip].to_place;
                    console.log(data.products[ip].to_place);
                    console.log($scope.items);
                };
                console.log($scope.items);

                $scope.customer = {
                    'name': data.customer_name,
                    'contact': data.customer_contact,
                    'address': data.customer_address,
                    'company': data.customer_company
                };

                $scope.invoice = {
                    'id': data.id,
                    'bill_no': data.bill_no,
                    'customer_id': data.customer_id,
                    'status_id': '',
                    'user_id': 1, //ADDING CONSTANT USER (CHANGE WHEN LOGIN INTEGRATED)
                    'date': $scope.date,
                    'type': data.type,
                    'gsttype': data.gsttype,
                    'paid': data.paid,
                    'amount': data.amount,
                    'service_tax': 0,
                    'swachhbharat_tax': 0,
                    'krishikalyancess_tax': 0,
                    'total': 0,
                    'tds': data.tds,
                    'customer_name': $scope.customer.name,
                    'from_date': $filter('date')(data.from_date, 'dd-MM-yyyy'),
                    'to_date': $filter('date')(data.to_date, 'dd-MM-yyyy')
                };
            };

            var getinvoiceerror = function (data, status) {
                console.log(data);
            };
            NavigationService.getinvoice(invoicenumber).success(getinvoicesuccess).error(getinvoiceerror);
        } else {

            //THINGS TO DO IF NOT IN EDIT MODE


            //SET BILL NUMBER
            var getlastbillnumbersuccess = function (data, status) {
                $scope.invoice.bill_no = data.bill_no;
                //REFINE BILL NUMBER
                $scope.invoice.bill_no = $scope.invoice.bill_no.substring(6, $scope.invoice.bill_no.length);
                $scope.invoice.bill_no = parseInt($scope.invoice.bill_no) + 1;
                $scope.invoice.bill_no = $scope.invoice.bill_no.toString();
                // 112
                if ($scope.invoice.bill_no.length < 5) {
                    var noz = 5 - $scope.invoice.bill_no.length;
                    for (z = 0; z < noz; z++) {
                        $scope.invoice.bill_no = '0' + $scope.invoice.bill_no;
                    };
                };
                $scope.invoice.bill_no = 'OMSAIG' + $scope.invoice.bill_no;
            };
            var getlastbillnumbererror = function (data, status) {
                $scope.invoice.bill_no = data.bill_no;
            };
            NavigationService.getlastbillnumber().success(getlastbillnumbersuccess).error(getlastbillnumbererror);

        };


        //ROUTES
        $scope.gotohome = function () {
            editinvoice = false;
            var invoicenumber = 0;
            var gohome = true;
            $location.path('/home');
        };


        //FUNCTIONS
        $scope.checkval = function () {
            $scope.invoice.from_date = $('input[name=fromdate]').val();
            $scope.invoice.to_date = $('input[name=todate]').val();
            console.log($scope.invoice.from_date);
            console.log($scope.invoice.to_date);
        };

        //GETTING LIST OF ALL CUSTOMER
        var getallcustomerssuccess = function (data, status) {
            $scope.customers = data;
        };
        var getallcustomerserror = function (data, status) {
            $scope.customers = false;
        };
        $scope.getallcustomers = function () {
            NavigationService.getallcustomers().success(getallcustomerssuccess).error(getallcustomerserror);
        };

        //PUT FILTER
        $scope.nametyped = function () {
            $scope.filter.company = $scope.customer.company;
            console.log($scope.filter.company);
        };

        //RESET FILTEER
        $scope.resetfilter = function () {
            $interval(function () {
                $scope.filter.company = null
            }, 200, 1);
        };

        //CUSTOMER SELECTED
        $scope.customerselected = function (cust) {
            $scope.filter.company = null;
            $scope.customer.name = cust.name;
            $scope.customer.contact = cust.contact;
            $scope.customer.address = cust.address;
            $scope.customer.company = cust.company;
            $scope.invoice.customer_id = cust.id;
            console.log($scope.invoice.customer_id);

        };



        //SAVE INDIVIDUAL PRODUCTS TO THE DATABASE
        var storeorderproductsuccess = function (data, status) {
            if ($scope.currentitem == $scope.items.length - 1) {
                $location.path('/invoice');
            };
        };
        var storeorderproducterror = function (data, status) {
            //ERROR MESSAGE
        };
        $scope.storeorderproducts = function (invid) {
            editinvoice = false;
            for (var i = 0; i < $scope.items.length; i++) {
                $scope.currentitem = i;
                $scope.items[i].invoice_id = invid;
                console.log($scope.items[i]);

                if ($scope.items[i].sync == false) {
                    NavigationService.storeorderproduct($scope.items[i]).success(storeorderproductsuccess).error(storeorderproducterror);
                } else {
                    NavigationService.editorderproduct($scope.items[i].id, $scope.items[i]).success(storeorderproductsuccess).error(storeorderproducterror);
                };
            };
        };

        //SAVE THE INVOICE TO DATABASE
        var saveinvoicesuccess = function (data, status) {
            console.log(data);
            if (editinvoice) {
                invoicenumber = $scope.invoice.id;
                console.log(invoicenumber);
            } else {
                invoicenumber = data;
            };
            gohome = true;

            if (editinvoice) {
                $scope.storeorderproducts($scope.invoice.id);
            } else {
                $scope.storeorderproducts(data);
            };
        };
        var saveinvoiceerror = function (data, status) {
            //ERROR MESSAGE
        };
        $scope.storeinvoice = function () {
            //SAVE INVOICE
            if (editinvoice) {
                NavigationService.editinvoice($scope.invoice.id, $scope.invoice).success(saveinvoicesuccess).error(saveinvoiceerror);
            } else {
                NavigationService.saveinvoice($scope.invoice).success(saveinvoicesuccess).error(saveinvoiceerror);
            };
        };

        //CHECKING IF CUSTOMER EXISTS OR TO ADD
        var checkcustomersuccess = function (data, status) {
            if (data == "false") {
                $scope.storeinvoice();
            } else {
                $scope.invoice.customer_id = parseInt(data);
                $scope.storeinvoice();
            };

        };
        var checkcustomererror = function (data, status) {
            //ERROR MESSAGE
        };
        $scope.checkcustomer = function () {
            NavigationService.checkcustomer($scope.customer).success(checkcustomersuccess).error(checkcustomererror);
        };

        //MAKE FULL PAYMENT
        $scope.makefullpayment = function () {
            $scope.invoice.paid = $scope.invoice.total - $scope.invoice.tds;
        };

        //SAVE THE INVOICE
        $scope.saveinvoice = function () {

            //GET DATE VALUE OF DATE PICKERS
            $scope.invoice.from_date = $('input[name=fromdate]').val();
            $scope.invoice.to_date = $('input[name=todate]').val();

            //CALCULATE TAXES AND INDIVIDUAL PRODUCT COSTS HERE
            $scope.invoice.service_tax = 0;
            $scope.invoice.swachhbharat_tax = 0;
            $scope.invoice.krishikalyancess_tax = 0;
            $scope.invoice.amount = 0;

            for (var r = 0; r < $scope.items.length; r++) {

                console.log($scope.items[r]);

                //ADJUST DATE FORMAT OF EACH PRODUCT
                $scope.items[r].date = $filter('date')($scope.items[r].date, 'yyyy-MM-dd');

                if ($scope.items[r].taxed) {
                    $scope.items[r].taxed = 1;
                    console.log($scope.items[r].carrier);
                    //TAKE OUT TAXES FROM EACH PRODUCT AND ADD TO TOTAL TAX
                    $scope.items[r].total = parseFloat($scope.items[r].total) / 1.18;
                    var st = (18 * parseFloat($scope.items[r].total)) / 100;
                    //var sbt = (0.5 / 115 * parseFloat($scope.items[r].total));
                    //var kct = (0.5 / 115 * parseFloat($scope.items[r].total));
                } else {
                    $scope.items[r].taxed = 0;
                    var st = 0;
                    // var sbt = 0;
                    //var kct = 0;
                    $scope.items[r].total = parseFloat($scope.items[r].total);
                };

                //REMOVE TAXED ELEMENT
                //delete $scope.items[r].taxed;

                $scope.invoice.service_tax += st;
                //$scope.invoice.swachhbharat_tax += sbt;
                //$scope.invoice.krishikalyancess_tax += kct;
                $scope.invoice.amount += $scope.items[r].total;
            };

            //SET CUSTOMER NAME IN INVOICE
            $scope.invoice.customer_name = $scope.customer.name;

            //SET DATE FORMAT
            var fromsplit = $scope.invoice.from_date.split("-");
            $scope.invoice.from_date = fromsplit[2] + "-" + fromsplit[1] + "-" + fromsplit[0];

            var tosplit = $scope.invoice.to_date.split("-");
            $scope.invoice.to_date = tosplit[2] + "-" + tosplit[1] + "-" + tosplit[0];

            //SET STATUS TO THE INVOICE
            if ($scope.invoice.paid < ($scope.invoice.total - $scope.invoice.tds)) {
                $scope.invoice.status_id = 2;
            } else {
                $scope.invoice.status_id = 1;
            };

            console.log($scope.invoice);
            console.log($scope.items);

            $scope.checkcustomer();
        };

        $scope.addtds = function () {
            /*if ($scope.invoice.paid > ($scope.invoice.total-$scope.invoice.tds) {
                $scope.invoice.paid = $scope.invoice.total-$scope.invoice.tds;
            };*/
        };
        //ADD ITEM TO INVOICE
        $scope.additem = function () {
            $scope.items.push({
                'product_id': '1',
                'date': '',
                'weight': 0,
                'unit_amount': 0,
                'pieces': 0,
                'from_place': '',
                'to_place': '',
                'reciever_contact': '',
                'carrier': '',
                'total': 0,
                'taxed': true,
                'sync': false
            });
        };

        //REMOVE ITEM FROM INVOICE
        $scope.removeitem = function (i) {
            $scope.items.splice(i, 1);
        };

        //REFRESH INVOICE VALS
        $scope.refresh = function () {
            $scope.$apply();
        };

        //FINDS TOTAL VAL
        $scope.totalval = function () {
            var tot = 0;
            for (var w = 0; w < $scope.items.length; w++) {
                tot += parseFloat($scope.items[w].total);
            };
            $scope.invoice.total = tot + $scope.invoice.service_tax + $scope.invoice.swachhbharat_tax + $scope.invoice.swachhbharat_tax;
        };

        $scope.assigntotal = function (val, i) {
            $scope.items[i].total = val;
        };



        //INITIAL FUNCTION CALLS
        $scope.getallcustomers();

    }
]);


phonecatControllers.controller('headerctrl', ['$scope', 'TemplateService', '$location',
    function ($scope, TemplateService, $location) {
        $scope.template = TemplateService;



    }
])
phonecatControllers.controller('invoiceCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location',
    function ($scope, TemplateService, NavigationService, $location) {
        $scope.template = TemplateService;
        $scope.navigation = NavigationService.getnav
        TemplateService.content = "views/invoice.html";

        $scope.taxedproducts = [];
        $scope.nontaxedproducts = [];
        $scope.taxableamount = 0;
        $scope.nontaxableamount = 0;

        /*FUNCTIONS*/

        /*GET INVOIVE DATA*/
        var getinvoicesuccess = function (data, status) {
            console.log(data);
            $scope.invoice = data;
            $scope.products = data.products;

            for (var t = 0; t < $scope.products.length; t++) {
                console.log(t);
                console.log($scope.products.length);
                if ($scope.products[t].taxed == "1") {
                    console.log("Taxed");
                    $scope.taxedproducts.push($scope.products[t]);
                    $scope.taxableamount += parseFloat($scope.products[t].total);
                } else {
                    console.log("Non Taxed");
                    $scope.nontaxedproducts.push($scope.products[t]);
                    $scope.nontaxableamount += parseFloat($scope.products[t].total);
                };
            };


            $scope.invoicetotal = parseFloat($scope.invoice.amount) + parseFloat($scope.invoice.service_tax) + parseFloat($scope.invoice.swachhbharat_tax) + parseFloat($scope.invoice.krishikalyancess_tax);
            console.log($scope.invoicetotal);
        };
        var getinvoiceerror = function (data, status) {
            $scope.invoice = data;
        };
        NavigationService.getinvoice(invoicenumber).success(getinvoicesuccess).error(getinvoiceerror);

        /*GET TOTAL AMOUNT OF ALL ITEMS - MIGHT NOT BE REQUIRED*/
        $scope.downloadpdf = function () {
            html2canvas(document.getElementById('exportthis'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500,
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download($scope.invoice.bill_no + ".pdf");
                }
            });
        };
        $scope.proceed = function () {
            if (gohome == true) {
                $location.path('/home');
            } else {
                $location.path('/reports');
            };
        };
    }
]);
