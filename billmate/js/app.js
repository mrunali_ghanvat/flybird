// JavaScript Document
var pixoloapp = angular.module('pixoloapp', [
    'ngRoute',
    'phonecatControllers',
    'templateservicemod',
    'navigationservice', 'chart.js', 'ngAutocomplete']);

var navs = [{
        appname: "login",
        name: "Login",
        classis: "active",
        subnav: [],
        sp: false
    },
    {
        appname: "create",
        name: "Login",
        classis: "active",
        subnav: [],
        sp: false
    },
    {
        appname: "home",
        name: "Home",
        classis: "inactive",
        subnav: [],
        sp: false
    },
    {
        appname: "stats",
        name: "Stats",
        classis: "inactive",
        subnav: [],
        sp: false
    },
    {
        appname: "reports",
        name: "Reports",
        classis: "inactive",
        subnav: [],
        sp: false
    },
    {
        appname: "addcustomer",
        name: "Add Customer",
        classis: "inactive",
        subnav: [],
        sp: false
    },
    {
        appname: "settings",
        name: "Settings",
        classis: "inactive",
        subnav: [],
        sp: false
    },
    {
        appname: "pixolo",
        name: "Pixolo",
        classis: "inactive",
        subnav: [],
        sp: false
    },
    {
        appname: "invoice",
        name: "Login",
        classis: "inactive",
        subnav: [],
        sp: false
    }, ];

pixoloapp.config(['$routeProvider',
    function ($routeProvider) {
        for (var i = 0; i < navs.length; i++) {
            if (navs[i].sp) {
                $routeProvider.
                when('/' + navs[i].appname, {
                    templateUrl: 'views/template.html',
                    controller: navs[i].appname + 'Ctrl'
                });
            } else {
                $routeProvider.
                when('/' + navs[i].appname, {
                    templateUrl: 'views/template.html',
                    controller: navs[i].appname + 'Ctrl'
                });
            };
            if (navs[i].subnav.length > 0) {
                for (var j = 0; j < navs[i].subnav.length; j++) {
                    $routeProvider.
                    when('/' + navs[i].subnav[j].appname, {
                        templateUrl: 'views/template.html',
                        controller: navs[i].subnav[j].appname + 'Ctrl'
                    });
                };
            };
        };
        $routeProvider.otherwise({
            redirectTo: window.localStorage.getItem('user') ? '/home' : '/login'
        });
    }
]);

pixoloapp.filter('decimal2', function () {
    return function (input) {
        return parseFloat(input).toFixed(2);
    };
})
pixoloapp.filter('capitalize', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
})
pixoloapp.filter('dateToISO', function () {
    return function (input) {
        input = new Date(input).toISOString();
        return input;
    };
})
pixoloapp.filter('words', function () {
    function isInteger(x) {
        return x % 1 === 0;
    }


    return function (value) {
        //value = $filter('number:0')(value);
        value = Math.round(value);
        if (value && isInteger(value))
            return toWords(value);

        return value;
    };

});

var th = ['', 'thousand', 'million', 'billion', 'trillion'];
var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];


function toWords(s) {

    s = s.toString();
    s = s.replace(/[\, ]/g, '');
    if (s != parseFloat(s)) return 'not a number';

    var x = s.indexOf('.');
    if (x == -1) x = s.length;
    if (x > 15) return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++) {
        if ((x - i) % 3 == 2) {
            if (n[i] == '1') {
                str += tn[Number(n[i + 1])] + ' ';
                i++;
                sk = 1;
            } else if (n[i] != 0) {
                str += tw[n[i] - 2] + ' ';
                sk = 1;
            }
        } else if (n[i] != 0) {
            str += dg[n[i]] + ' ';
            if ((x - i) % 3 == 0) str += 'hundred ';
            sk = 1;
        }


        if ((x - i) % 3 == 1) {
            if (sk) str += th[(x - i - 1) / 3] + ' ';
            sk = 0;
        }
    }
    if (x != s.length) {
        var y = s.length;
        str += 'point ';
        for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' ';
    }
    return str.replace(/\s+/g, ' ');
}

window.toWords = toWords;
